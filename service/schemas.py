from datetime import date
from typing import Optional

from fastapi_users.schemas import CreateUpdateDictModel
from pydantic import BaseModel, ConfigDict


class UserRead(BaseModel):
    full_name: str
    wage: int | None
    date_of_promotion: date | None

    model_config = ConfigDict(from_attributes=True)


class UserCreate(CreateUpdateDictModel):
    login: str
    password: str
    full_name: str


class UserUpdate(CreateUpdateDictModel):
    wage: Optional[int] = None
    date_of_promotion: Optional[date] = None
    is_superuser: Optional[bool] = None


class LoginCredentials(BaseModel):
    username: str
    password: str
