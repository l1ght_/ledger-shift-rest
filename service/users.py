from typing import Optional, Any

from fastapi import Depends, Request
from fastapi_users import BaseUserManager, FastAPIUsers, UUIDIDMixin, models, schemas, exceptions
from fastapi_users.authentication import (
    AuthenticationBackend,
    BearerTransport,
    JWTStrategy,
)

from service.conf import settings
from service.db import User, get_user_db, SQLAlchemyUserDatabase


SECRET = settings.SECRET_KEY
RESET_SECRET = settings.RESET_SECRET
VERIFICATION_SECRET = settings.VERIFICATION_SECRET


class UserManager(UUIDIDMixin, BaseUserManager[User, models.ID]):
    reset_password_token_secret = RESET_SECRET
    verification_token_secret = VERIFICATION_SECRET


    async def create(
        self,
        user_create: schemas.UC,
        safe: bool = False,
        request: Optional[Request] = None,
    ) -> models.UP:
        """
        Создание нового пользователя в БД. Переделка на свой лад с использованием login, вместо email.
        """
        await self.validate_password(user_create.password, user_create)

        existing_user = await self.user_db.get_by_email(user_create.login)
        if existing_user is not None:
            raise exceptions.UserAlreadyExists()

        user_dict = (
            user_create.create_update_dict()
            if safe
            else user_create.create_update_dict_superuser()
        )
        password = user_dict.pop("password")
        user_dict["hashed_password"] = self.password_helper.hash(password)

        created_user = await self.user_db.create(user_dict)

        await self.on_after_register(created_user, request)

        return created_user


    def parse_id(self, value: Any):
        '''Фикс отсутствия поддержки обычного ID(не UUID) в manager fastapi-users.'''
        return value


async def get_user_manager(user_db: SQLAlchemyUserDatabase = Depends(get_user_db)):
    yield UserManager(user_db)


bearer_transport = BearerTransport(tokenUrl="auth/jwt/login")


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=SECRET, lifetime_seconds=settings.TOKEN_LIFETIME_SECONDS)


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=bearer_transport,
    get_strategy=get_jwt_strategy,
)

fastapi_users = FastAPIUsers[User, models.ID](get_user_manager, [auth_backend])

current_user = fastapi_users.current_user()
current_super_user = fastapi_users.current_user(superuser=True)
