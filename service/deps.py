from typing import Union

from fastapi import Depends, HTTPException, status
from fastapi_users import BaseUserManager, models

from service import get_user_manager


async def get_user_by_id(
    id: int,
    user_manager: BaseUserManager[models.UP, models.ID] = Depends(get_user_manager)
) -> Union[models.UP, HTTPException]:
    user = await user_manager.get(id)
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return user
