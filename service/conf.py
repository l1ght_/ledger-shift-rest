from functools import cache

from pydantic import SecretStr
from pydantic_settings import BaseSettings


@cache
class Settings(BaseSettings):
    DB: str

    SECRET_KEY: SecretStr

    RESET_SECRET: SecretStr

    VERIFICATION_SECRET: SecretStr

    TOKEN_LIFETIME_SECONDS: int

    class Config:
        env_file = '.env'

settings = Settings()
