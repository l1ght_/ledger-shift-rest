from fastapi import Depends, APIRouter, status, Request, HTTPException
from fastapi_users import models, BaseUserManager, schemas, exceptions

from service import app
from service.db import User
from service.deps import get_user_by_id
from service.schemas import UserRead, UserUpdate
from service.users import current_user, get_user_manager


@app.get(
    "/me",
    tags=['user'],
    response_model=UserRead,
)
async def get_me(
        user: User = Depends(current_user),
):
    return schemas.model_validate(UserRead, user)


edit_router = APIRouter()


@edit_router.get(
    "/{id}",
    response_model=UserRead,
)
async def get_user(
        user: models.UP = Depends(get_user_by_id),
):
    return schemas.model_validate(UserRead, user)


@edit_router.patch(
    "/{id}",
    response_model=UserUpdate,
)
async def update_user(
    request: Request,
    updated_user: UserUpdate,
    user: models.UP = Depends(get_user_by_id),
    user_manager: BaseUserManager[models.UP, models.ID] = Depends(get_user_manager),
):
    try:
        user = await user_manager.update(
            updated_user, user, safe=False, request=request
        )
        return schemas.model_validate(UserRead, user)
    except exceptions.UserNotExists():
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)
    except:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)



@edit_router.delete(
    "/{id}",
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_user(
    request: Request,
    user: models.UP = Depends(get_user_by_id),
    user_manager: BaseUserManager[models.UP, models.ID] = Depends(get_user_manager),
):
    await user_manager.delete(user, request=request)
    return None
