from datetime import date
from typing import AsyncGenerator, Optional

from fastapi import Depends
from fastapi_users import models
from fastapi_users.db import SQLAlchemyUserDatabase as BaseSQLAlchemyUserDatabase
from fastapi_users.models import UP
from sqlalchemy import Column, Integer, String, Boolean, select, func
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine, async_sessionmaker
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from service.conf import settings


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "user"

    id: models.ID = Column(Integer, primary_key=True)
    login: Mapped[str] = mapped_column(String(length=100), unique=True, nullable=False)
    hashed_password: Mapped[str] = mapped_column(String(length=1024), nullable=False)
    is_superuser: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)

    full_name: Mapped[str] = mapped_column(String(length=100), nullable=False)
    wage: Mapped[int | None]
    date_of_promotion: Mapped[date | None]


engine = create_async_engine(settings.DB)
async_session_maker = async_sessionmaker(
    engine,
    class_=AsyncSession,
    expire_on_commit=False
)


class SQLAlchemyUserDatabase(BaseSQLAlchemyUserDatabase):
    async def get_by_email(self, login: str) -> Optional[UP]:
        statement = select(self.user_table).where(
            func.lower(self.user_table.login) == func.lower(login)
        )
        return await self._get_user(statement)


async def create_db_and_tables():
    '''
    Create_all надо избегать и работать с уже созданной БД,
    но для данного задания это буде overhead, поэтому оставлю.
    '''
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


async def get_user_db(session: AsyncSession = Depends(get_async_session)):
    if session is None:
        session = get_async_session()
    yield SQLAlchemyUserDatabase(session, User)


